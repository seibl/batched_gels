/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <cooperative_groups.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>
#include <fmt/format.h>

#include <iostream>

#include "datatypes.hpp"

struct BlasCublas
{
    using BlasHandle = cublasHandle_t;

    static BlasHandle create();
    static void destroy(BlasHandle& handle);

    static int gelsBatched(BlasHandle handle,
                           BatchedMatrix scratchMatrices,
                           BatchedPointers batchedMatrices,
                           BatchedVector scratchRHS,
                           BatchedPointers batchedRHS,
                           const int numModels,
                           const int numFeatures,
                           const int numSamples,
                           const int batchStartIndex,
                           DevInfo devInfo);
};
