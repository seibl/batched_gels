#include "blas_cuda.cuh"

#include <cooperative_groups.h>
#include <cooperative_groups/memcpy_async.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>

#include <algorithm>
#include <cassert>
#include <stdexcept>

// https://developer.nvidia.com/blog/cooperative-groups/
namespace cg = cooperative_groups;

__device__ double reduce(double val)
{
    __shared__ double ret;

    cg::thread_block g = cg::this_thread_block();

    auto tile32 = cg::tiled_partition<32>(g);
    for (int i = tile32.size() / 2; i > 0; i /= 2)
    {
        val += tile32.shfl_down(val, i);
    }

    if (g.thread_rank() == 0)
    {
        ret = 0;
    }
    g.sync();
    if (tile32.thread_rank() == 0)
    {
        atomicAdd(&ret, val);
    }
    g.sync();
    return ret;
}

__device__ constexpr double sign(const double& v) { return v >= 0 ? 1.0 : -1.0; }

__global__ void customDgelsBatched(const int M,
                                   const int N,
                                   double* const Aarray[], /*Device pointer*/
                                   const int lda,
                                   double* const Carray[], /*Device pointer*/
                                   const int batchSize)
{
    assert(lda >= M);

    // const int rowIdx = threadIdx.x;
    const int stride = blockDim.x;
    const int batchIdx = blockIdx.y * gridDim.x + blockIdx.x;

    double* A = Aarray[batchIdx];
    extern __shared__ double v[];
    double* b = Carray[batchIdx];

    cg::thread_block this_block = cg::this_thread_block();

    for (auto columnIdx = 0; columnIdx < N; ++columnIdx)
    {
        double* Acolumn = &A[columnIdx * lda];

        // copy column into v
        double sumSquared = 0;
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            v[rowIdx] = rowIdx >= columnIdx ? Acolumn[rowIdx] : 0.0;
            sumSquared += v[rowIdx] * v[rowIdx];
        }

        // modify diagonal element
        sumSquared = reduce(sumSquared);
        double sumSquared2 = 0;
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            if (rowIdx == columnIdx)  // only one element needs to be changed
            {
                v[columnIdx] += sign(v[columnIdx]) * sqrt(sumSquared);
            }
            sumSquared2 += v[rowIdx] * v[rowIdx];
        }

        // normalize
        sumSquared2 = reduce(sumSquared2);
        auto invNorm = rsqrt(sumSquared2);
        double dotB = 0;
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            v[rowIdx] *= invNorm;
            dotB += v[rowIdx] * b[rowIdx];
        }

        // calc Q* b
        dotB = reduce(dotB);
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            if (rowIdx >= columnIdx)
            {
                b[rowIdx] -= 2.0 * v[rowIdx] * dotB;
            }
        }

        for (auto innerColumnIdx = columnIdx; innerColumnIdx < N; ++innerColumnIdx)
        {
            double* AInnerColumn = &A[innerColumnIdx * lda];

            double v_dot_A = 0;
            for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
            {
                v_dot_A += v[rowIdx] * AInnerColumn[rowIdx];
            }
            // dot product to get length
            v_dot_A = reduce(v_dot_A);

            // subtract twice
            for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
            {
                AInnerColumn[rowIdx] -= 2.0 * v[rowIdx] * v_dot_A;
            }
        }
    }

    // back substitution
    for (auto columnIdx = N - 1; columnIdx >= 0; --columnIdx)
    {
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            if (rowIdx == columnIdx)
            {
                b[rowIdx] /= A[rowIdx + columnIdx * lda];
            }
        }
        this_block.sync();
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            if (rowIdx < columnIdx)
            {
                b[rowIdx] -= b[columnIdx] * A[rowIdx + columnIdx * lda];
            }
        }
    }
}

BlasCuda::BlasHandle BlasCuda::create() { return 0; }
void BlasCuda::destroy(BlasCuda::BlasHandle& /*handle*/) {}

int BlasCuda::gelsBatched(BlasHandle handle,
                          BatchedMatrix scratchMatrices,
                          BatchedPointers batchedMatrices,
                          BatchedVector scratchRHS,
                          BatchedPointers batchedRHS,
                          const int numModels,
                          const int numFeatures,
                          const int numSamples,
                          const int batchStartIndex,
                          DevInfo devInfo)
{
    if (numSamples > 4096) return EXIT_FAILURE;

    auto trans = 'N';
    auto m = numSamples;
    auto n = numFeatures;
    auto nrhs = 1;
    auto Aarray = reinterpret_cast<double**>(&batchedMatrices(batchStartIndex));
    int lda = scratchMatrices.stride_1();
    auto Carray = reinterpret_cast<double**>(&batchedRHS(batchStartIndex));
    int ldc = scratchRHS.stride_1();
    int info = 0;
    auto batchSize = numModels;

    auto threads = std::min(m, 256);
    customDgelsBatched<<<batchSize, threads, sizeof(double) * m>>>(
        m, n, Aarray, lda, Carray, batchSize);

    cudaDeviceSynchronize();
    return EXIT_SUCCESS;
}
