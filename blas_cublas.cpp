/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include "blas_cublas.hpp"

#define CHECK_CUDA_ERROR(ans)                        \
    {                                                \
        check_cuda_error((ans), __FILE__, __LINE__); \
    }
inline void check_cuda_error(cudaError_t code, const char* file, int line, bool abort = true)
{
    if (code != cudaSuccess)
    {
        std::cerr << fmt::format("CUDA ERROR: {} {} {}", cudaGetErrorString(code), file, line)
                  << std::endl;
        if (abort) exit(code);
    }
}

#define CHECK_CUBLAS_ERROR(ans)                        \
    {                                                  \
        check_cublas_error((ans), __FILE__, __LINE__); \
    }
inline void check_cublas_error(cublasStatus_t code, const char* file, int line, bool abort = true)
{
    if (code != CUBLAS_STATUS_SUCCESS)
    {
        std::cerr << "CUDA ERROR: " << code << " " << file << " " << line << std::endl;
        if (abort) exit(code);
    }
}

BlasCublas::BlasHandle BlasCublas::create()
{
    BlasCublas::BlasHandle handle;
    auto stat = cublasCreate(&handle);
    if (stat != CUBLAS_STATUS_SUCCESS)
    {
        throw std::runtime_error("CUBLAS initialization failed\n");
    }
    return handle;
}

void BlasCublas::destroy(BlasCublas::BlasHandle& handle) { cublasDestroy(handle); }

int BlasCublas::gelsBatched(BlasCublas::BlasHandle handle,
                            BatchedMatrix scratchMatrices,
                            BatchedPointers batchedMatrices,
                            BatchedVector scratchRHS,
                            BatchedPointers batchedRHS,
                            const int numModels,
                            const int numFeatures,
                            const int numSamples,
                            const int batchStartIndex,
                            DevInfo devInfo)
{
    auto m = numSamples;
    auto n = numFeatures;
    auto nrhs = 1;
    auto Aarray = reinterpret_cast<double**>(&batchedMatrices(batchStartIndex));
    int lda = scratchMatrices.stride_1();
    auto Carray = reinterpret_cast<double**>(&batchedRHS(batchStartIndex));
    int ldc = scratchRHS.stride_1();
    int info = 0;
    auto batchSize = numModels;

    auto status = cublasDgelsBatched(handle,
                                     CUBLAS_OP_N,
                                     m,
                                     n,
                                     nrhs,
                                     Aarray,
                                     lda,
                                     Carray,
                                     ldc,
                                     &info,
                                     devInfo.data(),
                                     batchSize);
    cudaDeviceSynchronize();
    return status;
}
