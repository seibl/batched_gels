/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <Kokkos_Macros.hpp>

#include "blas_cpu.hpp"
#include "blas_kokkos.hpp"

#if defined(KOKKOS_ENABLE_CUDA)
#include "blas_cublas.hpp"
#include "blas_cuda.cuh"
#include "blas_cuda_shared.hpp"
#elif defined(KOKKOS_ENABLE_HIP)
#include "blas_hip.hpp"
#include "blas_rocsolver.hpp"
#endif
