/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <Kokkos_Core.hpp>

struct WrappedPointer
{
    double *ptr;
};
static_assert(sizeof(WrappedPointer) == sizeof(double *));

using MemorySpace = Kokkos::DefaultExecutionSpace::memory_space;

using BatchedPointers = Kokkos::View<WrappedPointer *, MemorySpace>;
using BatchedMatrix = Kokkos::View<double ***, Kokkos::LayoutLeft, MemorySpace>;
using BatchedVector = Kokkos::View<double **, Kokkos::LayoutLeft, MemorySpace>;
using DevInfo = Kokkos::View<int *, MemorySpace>;