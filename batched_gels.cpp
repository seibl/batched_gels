/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include <fmt/format.h>
#include <mpi.h>
#include <omp.h>

#include <Kokkos_Core.hpp>
#include <Kokkos_Random.hpp>
#include <Kokkos_Timer.hpp>
#include <iostream>
#include <vector>

#include "blas.hpp"

// dimensions
constexpr int M = 2048;
constexpr int N = 3;
constexpr int RHS = 1;
constexpr int iterations = 1;
int comm_size;
int comm_rank;

template <class SOLVER>
void batched_gels(int BATCH_SIZE, int DATASET_SIZE)
{
    auto A = BatchedMatrix(Kokkos::ViewAllocateWithoutInitializing("A"), M, N, DATASET_SIZE);
    auto b = BatchedVector(Kokkos::ViewAllocateWithoutInitializing("b"), M, DATASET_SIZE);
    auto batched_A =
        BatchedPointers(Kokkos::ViewAllocateWithoutInitializing("batched_A"), DATASET_SIZE);
    auto batched_b =
        BatchedPointers(Kokkos::ViewAllocateWithoutInitializing("batched_b"), DATASET_SIZE);
    auto dev_info = DevInfo("dev_info", DATASET_SIZE);

    for (size_t idx = 0; idx < DATASET_SIZE; ++idx)
    {
        batched_A(idx).ptr = &A(0, 0, idx);
        batched_b(idx).ptr = &b(0, idx);
    }

    auto policy = Kokkos::RangePolicy<>(0, DATASET_SIZE);
    auto kernel = KOKKOS_LAMBDA(const int b_idx)
    {
        for (int m_idx = 0; m_idx < M; ++m_idx)
        {
            for (int n_idx = 0; n_idx < N; ++n_idx)
            {
                A(m_idx, n_idx, b_idx) = m_idx == n_idx ? m_idx + 1 : 0;
            }
            b(m_idx, b_idx) = (m_idx + 1) * (m_idx + 1);
        }
    };
    Kokkos::parallel_for("init_random", policy, kernel);
    Kokkos::fence();

    auto handle = SOLVER::create();
    MPI_Barrier(MPI_COMM_WORLD);
    Kokkos::Timer timer;
    for (auto batch_start_idx = 0; batch_start_idx < DATASET_SIZE; batch_start_idx += BATCH_SIZE)
    {
        for (auto iter = 0; iter < iterations; ++iter)
        {
            //            blas::gels_batched(handle,
            //                               'N',
            //                               M,
            //                               N,
            //                               RHS,
            //                               reinterpret_cast<double
            //                               **>(&batched_A(batch_start_idx)), lda,
            //                               reinterpret_cast<double
            //                               **>(&batched_b(batch_start_idx)), ldb, &info,
            //                               dev_info.data(),
            //                               BATCH_SIZE);
            //            custom_gels_batched(M,
            //                                N,
            //                                reinterpret_cast<double
            //                                **>(&batched_A(batch_start_idx)), lda,
            //                                reinterpret_cast<double
            //                                **>(&batched_b(batch_start_idx)), BATCH_SIZE);
            //            blas_kokkos::gels_batched(A, b, DATASET_SIZE, N, M);
            SOLVER::gelsBatched(
                handle, A, batched_A, b, batched_b, DATASET_SIZE, N, M, batch_start_idx, dev_info);
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    SOLVER::destroy(handle);

    if (comm_rank == 0)
    {
        auto time = timer.seconds();
        std::cout << fmt::format("dataset: {}, batch: {}, iterations: {}, time: {} s",
                                 DATASET_SIZE,
                                 BATCH_SIZE,
                                 iterations,
                                 time)
                  << std::endl;
        std::cout << fmt::format(
                         "performance: {} MSys/s",
                         1e-6 * comm_size * iterations * static_cast<double>(DATASET_SIZE) / time)
                  << std::endl;
    }
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    Kokkos::initialize(argc, argv);
    if (comm_rank == 0)
    {
        std::cout << "execution space: " << typeid(Kokkos::DefaultExecutionSpace).name()
                  << std::endl;
        std::cout << "memory space: " << typeid(MemorySpace).name() << std::endl;
        std::cout << fmt::format("ranks: {}, threads: {}", comm_size, omp_get_max_threads())
                  << std::endl;
        std::cout << fmt::format("matrix size: {}x{}", M, N) << std::endl;
    }

    std::vector<int> batch_sizes{
        /*128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536 ,*/ 131072 /*,
        /*262144 , 524288*/};
    for (auto batch_size : batch_sizes)
    {
        std::cout << "*** BlasCPU ***" << std::endl;
        batched_gels<BlasCPU>(batch_size, batch_sizes.back());
        batched_gels<BlasCPU>(batch_size, batch_sizes.back());
        batched_gels<BlasCPU>(batch_size, batch_sizes.back());

        std::cout << "*** BlasKokkos ***" << std::endl;
        batched_gels<BlasKokkos>(batch_size, batch_sizes.back());
        batched_gels<BlasKokkos>(batch_size, batch_sizes.back());
        batched_gels<BlasKokkos>(batch_size, batch_sizes.back());

#if defined(KOKKOS_ENABLE_CUDA)
        std::cout << "*** BlasCuda ***" << std::endl;
        batched_gels<BlasCuda>(batch_size, batch_sizes.back());
        batched_gels<BlasCuda>(batch_size, batch_sizes.back());
        batched_gels<BlasCuda>(batch_size, batch_sizes.back());

        std::cout << "*** BlasCublas ***" << std::endl;
        batched_gels<BlasCublas>(batch_size, batch_sizes.back());
        batched_gels<BlasCublas>(batch_size, batch_sizes.back());
        batched_gels<BlasCublas>(batch_size, batch_sizes.back());

        std::cout << "*** BlasCudaShared ***" << std::endl;
        batched_gels<BlasCudaShared>(batch_size, batch_sizes.back());
        batched_gels<BlasCudaShared>(batch_size, batch_sizes.back());
        batched_gels<BlasCudaShared>(batch_size, batch_sizes.back());
#endif

#if defined(KOKKOS_ENABLE_HIP)
        std::cout << "*** BlasHIP ***" << std::endl;
        batched_gels<BlasHIP>(batch_size, batch_sizes.back());
        batched_gels<BlasHIP>(batch_size, batch_sizes.back());
        batched_gels<BlasHIP>(batch_size, batch_sizes.back());

        std::cout << "*** BlasRocSolver ***" << std::endl;
        batched_gels<BlasRocSolver>(batch_size, batch_sizes.back());
        batched_gels<BlasRocSolver>(batch_size, batch_sizes.back());
        batched_gels<BlasRocSolver>(batch_size, batch_sizes.back());
#endif
    }

    Kokkos::finalize();
    MPI_Finalize();
    return EXIT_SUCCESS;
}
