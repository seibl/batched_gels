#include <fmt/format.h>

#include <Kokkos_Core.hpp>
#include <Kokkos_Random.hpp>
#include <Kokkos_Timer.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_session.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>

#include "blas.hpp"

#if defined(KOKKOS_ENABLE_CUDA)
#define ADDITIONAL_TESTS , BlasCuda, BlasCublas, BlasCudaShared
#elif defined(KOKKOS_ENABLE_HIP)
#define ADDITIONAL_TESTS , BlasHIP, BlasRocSolver
#else
#define ADDITIONAL_TESTS
#endif

TEMPLATE_TEST_CASE("example matrices", "[dgels]", BlasCPU, BlasKokkos ADDITIONAL_TESTS)
{
    constexpr int M = 4;
    constexpr int N = 3;
    constexpr int DATASET_SIZE = 1;

    auto A = BatchedMatrix("A", M, N, DATASET_SIZE);
    auto b = BatchedVector("b", M, DATASET_SIZE);
    auto v = BatchedVector("v", M, DATASET_SIZE);
    auto batched_A =
        BatchedPointers(Kokkos::ViewAllocateWithoutInitializing("batched_A"), DATASET_SIZE);
    auto batched_b =
        BatchedPointers(Kokkos::ViewAllocateWithoutInitializing("batched_b"), DATASET_SIZE);
    auto dev_info = DevInfo("dev_info", DATASET_SIZE);

    for (size_t idx = 0; idx < DATASET_SIZE; ++idx)
    {
        batched_A(idx).ptr = &A(0, 0, idx);
        batched_b(idx).ptr = &b(0, idx);
    }

    SECTION("back substitution")
    {
        for (auto batchIdx = 0; batchIdx < DATASET_SIZE; ++batchIdx)
        {
            A(0, 0, batchIdx) = 2;
            A(1, 0, batchIdx) = 0;
            A(2, 0, batchIdx) = 0;
            A(3, 0, batchIdx) = 0;

            A(0, 1, batchIdx) = 0;
            A(1, 1, batchIdx) = 4;
            A(2, 1, batchIdx) = 0;
            A(3, 1, batchIdx) = 0;

            A(0, 2, batchIdx) = 0;
            A(1, 2, batchIdx) = 0;
            A(2, 2, batchIdx) = -4;
            A(3, 2, batchIdx) = 0;

            b(0, batchIdx) = 1;
            b(1, batchIdx) = 1;
            b(2, batchIdx) = 1;
            b(3, batchIdx) = 1;
        }

        auto handle = TestType::create();
        TestType::gelsBatched(handle, A, batched_A, b, batched_b, DATASET_SIZE, N, M, 0, dev_info);
        TestType::destroy(handle);

        CHECK(b(0, 0) == +0.5);
        CHECK(b(1, 0) == +0.25);
        CHECK(b(2, 0) == -0.25);
    }

    SECTION("explicit checks")
    {
        for (auto batchIdx = 0; batchIdx < DATASET_SIZE; ++batchIdx)
        {
            A(0, 0, batchIdx) = 12;
            A(1, 0, batchIdx) = 6;
            A(2, 0, batchIdx) = -4;

            A(0, 1, batchIdx) = -51;
            A(1, 1, batchIdx) = 167;
            A(2, 1, batchIdx) = 24;

            A(0, 2, batchIdx) = 4;
            A(1, 2, batchIdx) = -68;
            A(2, 2, batchIdx) = -41;

            b(0, batchIdx) = 1;
            b(1, batchIdx) = 1;
            b(2, batchIdx) = 1;
            b(3, batchIdx) = 1;
        }

        auto handle = TestType::create();
        TestType::gelsBatched(handle, A, batched_A, b, batched_b, DATASET_SIZE, N, M, 0, dev_info);
        TestType::destroy(handle);

        SECTION("checking A")
        {
            CHECK(A(0, 0, 0) == Catch::Approx(-14));
            //            CHECK(A(1, 0, 0) ==
            //                  Catch::Approx(0).margin(std::numeric_limits<double>::epsilon() *
            //                  100));
            //            CHECK(A(2, 0, 0) ==
            //                  Catch::Approx(0).margin(std::numeric_limits<double>::epsilon() *
            //                  100));

            CHECK(A(0, 1, 0) == Catch::Approx(-21));
            CHECK(A(1, 1, 0) == Catch::Approx(-175));
            //            CHECK(A(2, 1, 0) ==
            //                  Catch::Approx(0).margin(std::numeric_limits<double>::epsilon() *
            //                  100));

            CHECK(A(0, 2, 0) == Catch::Approx(14));
            CHECK(A(1, 2, 0) == Catch::Approx(70));
            CHECK(std::abs(A(2, 2, 0)) == Catch::Approx(35));
        }
        SECTION("checking b")
        {
            CHECK(b(0, 0) == Catch::Approx(0.05142857));
            CHECK(b(1, 0) == Catch::Approx(-0.01028571));
            CHECK(b(2, 0) == Catch::Approx(-0.03542857));
        }
    }
}

template <typename TestType, int M, int N, int DATASET_SIZE>
void batched_gels()
{
    std::cout << fmt::format("matrix size: {}x{}x{}", M, N, DATASET_SIZE) << std::endl;

    auto A_cublas =
        BatchedMatrix(Kokkos::ViewAllocateWithoutInitializing("A_cublas"), M, N, DATASET_SIZE);
    auto b_cublas =
        BatchedVector(Kokkos::ViewAllocateWithoutInitializing("b_cublas"), M, DATASET_SIZE);
    auto batched_A_cublas =
        BatchedPointers(Kokkos::ViewAllocateWithoutInitializing("batched_A_cublas"), DATASET_SIZE);
    auto batched_b_cublas =
        BatchedPointers(Kokkos::ViewAllocateWithoutInitializing("batched_b_cublas"), DATASET_SIZE);
    auto dev_info = DevInfo("dev_info", DATASET_SIZE);

    auto A_custom =
        BatchedMatrix(Kokkos::ViewAllocateWithoutInitializing("A_custom"), M, N, DATASET_SIZE);
    auto b_custom =
        BatchedVector(Kokkos::ViewAllocateWithoutInitializing("b_custom"), M, DATASET_SIZE);
    auto batched_A_custom =
        BatchedPointers(Kokkos::ViewAllocateWithoutInitializing("batched_A_custom"), DATASET_SIZE);
    auto batched_b_custom =
        BatchedPointers(Kokkos::ViewAllocateWithoutInitializing("batched_b_custom"), DATASET_SIZE);

    for (size_t idx = 0; idx < DATASET_SIZE; ++idx)
    {
        batched_A_cublas(idx).ptr = &A_cublas(0, 0, idx);
        batched_b_cublas(idx).ptr = &b_cublas(0, idx);

        batched_A_custom(idx).ptr = &A_custom(0, 0, idx);
        batched_b_custom(idx).ptr = &b_custom(0, idx);
    }

    std::default_random_engine rng(1234);
    for (auto b_idx = 0; b_idx < DATASET_SIZE; ++b_idx)
    {
        for (int m_idx = 0; m_idx < M; ++m_idx)
        {
            for (int n_idx = 0; n_idx < N; ++n_idx)
            {
                A_cublas(m_idx, n_idx, b_idx) = rng() / double(rng.max());
            }
            b_cublas(m_idx, b_idx) = rng() / double(rng.max());
        }
    }

    Kokkos::deep_copy(A_custom, A_cublas);
    Kokkos::deep_copy(b_custom, b_cublas);

    int info;

    auto handle = BlasCPU::create();
    info = BlasCPU::gelsBatched(handle,
                                A_cublas,
                                batched_A_cublas,
                                b_cublas,
                                batched_b_cublas,
                                DATASET_SIZE,
                                N,
                                M,
                                0,
                                dev_info);
    BlasCPU::destroy(handle);

    REQUIRE(info == 0);
    for (int batch_idx = 0; batch_idx < DATASET_SIZE; ++batch_idx)
    {
        REQUIRE(dev_info(batch_idx) == 0);
    }

    auto handle2 = TestType::create();
    info = TestType::gelsBatched(handle2,
                                 A_custom,
                                 batched_A_custom,
                                 b_custom,
                                 batched_b_custom,
                                 DATASET_SIZE,
                                 N,
                                 M,
                                 0,
                                 dev_info);
    TestType::destroy(handle2);

    double maxAdifference = 0;
    double maxbdifference = 0;

    for (int batch_idx = 0; batch_idx < DATASET_SIZE; ++batch_idx)
    {
        for (int m_idx = 0; m_idx < M; ++m_idx)
        {
            for (int n_idx = 0; n_idx < N; ++n_idx)
            {
                if (m_idx <= n_idx)
                {
                    maxAdifference = std::max(maxAdifference,
                                              std::abs(A_cublas(m_idx, n_idx, batch_idx) -
                                                       A_custom(m_idx, n_idx, batch_idx)));
                    //                    std::cout << fmt::format("{}x{}x{}", m_idx, n_idx,
                    //                    batch_idx) << std::endl;
                    REQUIRE(A_cublas(m_idx, n_idx, batch_idx) ==
                            Catch::Approx(A_custom(m_idx, n_idx, batch_idx)));
                }
            }
            if (m_idx < N)
            {
                maxbdifference =
                    std::max(maxbdifference,
                             std::abs(b_cublas(m_idx, batch_idx) - b_custom(m_idx, batch_idx)));
                //                std::cout << fmt::format("{}x{}", m_idx, batch_idx) << std::endl;
                REQUIRE(b_cublas(m_idx, batch_idx) == Catch::Approx(b_custom(m_idx, batch_idx)));
            }
        }
    }

    std::cout << "max A difference: " << maxAdifference << std::endl;
    std::cout << "max b difference: " << maxbdifference << std::endl;
}

TEMPLATE_TEST_CASE("validation against cublas", "[dgels]", BlasCPU, BlasKokkos ADDITIONAL_TESTS)
{
    batched_gels<TestType, 32, 3, 2048>();
    batched_gels<TestType, 64, 3, 2048>();
    batched_gels<TestType, 512, 2, 2048>();
    batched_gels<TestType, 1024, 2, 2048>();
    batched_gels<TestType, 512, 3, 2048>();
    batched_gels<TestType, 1024, 3, 2048>();
    // batched_gels<TestType, 2048, 3, 2048>(handle);
}

int main(int argc, char *argv[])
{
    Kokkos::initialize(argc, argv);

    std::cout << "execution space: " << typeid(Kokkos::DefaultExecutionSpace).name() << std::endl;
    std::cout << "memory space: " << typeid(Kokkos::DefaultExecutionSpace::memory_space).name()
              << std::endl;

    int result = Catch::Session().run(argc, argv);

    Kokkos::finalize();

    return result;
}
