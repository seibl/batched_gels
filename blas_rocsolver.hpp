/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <fmt/format.h>

#include <iostream>

#include "datatypes.hpp"
#include <hip/hip_runtime.h>
#include <rocsolver.h>

struct BlasRocSolver
{
    using BlasHandle = rocblas_handle;

    static BlasHandle create();
    static void destroy(BlasHandle& handle);

    static int gelsBatched(BlasHandle handle,
                           BatchedMatrix scratchMatrices,
                           BatchedPointers batchedMatrices,
                           BatchedVector scratchRHS,
                           BatchedPointers batchedRHS,
                           const int numModels,
                           const int numFeatures,
                           const int numSamples,
                           const int batchStartIndex,
                           DevInfo devInfo);
};
