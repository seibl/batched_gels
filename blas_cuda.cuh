#pragma once

#include "datatypes.hpp"

struct BlasCuda
{
    using BlasHandle = int;

    static BlasHandle create();
    static void destroy(BlasHandle& handle);

    static int gelsBatched(BlasHandle handle,
                           BatchedMatrix scratchMatrices,
                           BatchedPointers batchedMatrices,
                           BatchedVector scratchRHS,
                           BatchedPointers batchedRHS,
                           const int numModels,
                           const int numFeatures,
                           const int numSamples,
                           const int batchStartIndex,
                           DevInfo devInfo);
};