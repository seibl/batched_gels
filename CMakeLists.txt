cmake_minimum_required(VERSION 3.20)

project(batched_gels LANGUAGES CXX)

include(CMakePrintHelpers)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_executable(batched_gels)
add_executable(batched_gels_test)

include(FetchContent)
if (Kokkos_ENABLE_CUDA)
    find_package(CUDAToolkit REQUIRED)
    target_link_libraries(batched_gels PUBLIC CUDA::cudart CUDA::cublas)
    target_link_libraries(batched_gels_test PUBLIC CUDA::cudart CUDA::cublas)

    set(Kokkos_ENABLE_CUDA_CONSTEXPR ON CACHE BOOL "" FORCE)
    set(Kokkos_ENABLE_CUDA_LAMBDA ON CACHE BOOL "" FORCE)
    set(Kokkos_ENABLE_CUDA_UVM ON CACHE BOOL "" FORCE)
endif ()

if (Kokkos_ENABLE_HIP)
    find_package(hip REQUIRED)
    target_link_libraries(batched_gels PUBLIC hip::device)
    target_link_libraries(batched_gels_test PUBLIC hip::device)

    find_package(rocsolver REQUIRED)
    target_link_libraries(batched_gels PUBLIC roc::rocsolver)
    target_link_libraries(batched_gels_test PUBLIC roc::rocsolver)
endif ()

set(Kokkos_ENABLE_SERIAL ON CACHE BOOL "" FORCE)

FetchContent_Declare(
        Kokkos
        GIT_REPOSITORY https://github.com/kokkos/kokkos.git
        GIT_TAG 3.7.00
)
FetchContent_MakeAvailable(Kokkos)

FetchContent_Declare(
        FMT
        GIT_REPOSITORY https://github.com/fmtlib/fmt.git
        GIT_TAG 8.1.0
)
FetchContent_MakeAvailable(FMT)

FetchContent_Declare(
  Catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2.git
  GIT_TAG        v3.0.1
)

FetchContent_MakeAvailable(Catch2)

include(CheckTypeSize)
check_type_size(int INT_SIZE BUILTIN_TYPES_ONLY LANGUAGE CXX)
if (NOT ${INT_SIZE} STREQUAL "4")
    message(FATAL_ERROR "Wrong int size")
endif()

find_package(BLAS REQUIRED)
find_package(LAPACK REQUIRED)
find_package(OpenMP REQUIRED)
find_package(MPI REQUIRED CXX)

if (Kokkos_ENABLE_CUDA)
    list(APPEND ADDITIONAL_SOURCES blas_cublas.cpp blas_cublas.cpp blas_cuda.cuh blas_cuda.cpp blas_cuda_shared.cpp blas_cuda_shared.hpp)
endif()

if (Kokkos_ENABLE_HIP)
    list(APPEND ADDITIONAL_SOURCES blas_hip.hpp blas_hip.cpp blas_rocsolver.cpp blas_rocsolver.hpp)
endif()

message(STATUS ${ADDITIONAL_SOURCES})

target_sources(batched_gels PRIVATE batched_gels.cpp blas_cpu.hpp blas_cpu.cpp blas_kokkos.hpp blas_kokkos.cpp)
target_sources(batched_gels PRIVATE ${ADDITIONAL_SOURCES})
target_link_libraries(batched_gels PRIVATE Kokkos::kokkos)
target_link_libraries(batched_gels PRIVATE BLAS::BLAS LAPACK::LAPACK)
target_link_libraries(batched_gels PRIVATE OpenMP::OpenMP_CXX)
target_link_libraries(batched_gels PRIVATE MPI::MPI_CXX)
target_link_libraries(batched_gels PRIVATE fmt::fmt)

target_sources(batched_gels_test PRIVATE batched_gels.test.cpp blas_cpu.hpp blas_cpu.cpp blas_kokkos.hpp blas_kokkos.cpp)
target_sources(batched_gels_test PRIVATE ${ADDITIONAL_SOURCES})
target_link_libraries(batched_gels_test PRIVATE Catch2::Catch2)
target_link_libraries(batched_gels_test PRIVATE Kokkos::kokkos)
target_link_libraries(batched_gels_test PRIVATE BLAS::BLAS LAPACK::LAPACK)
target_link_libraries(batched_gels_test PRIVATE OpenMP::OpenMP_CXX)
target_link_libraries(batched_gels_test PRIVATE MPI::MPI_CXX)
target_link_libraries(batched_gels_test PRIVATE fmt::fmt)
