/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include "blas_kokkos.hpp"

#include <stdexcept>

KOKKOS_INLINE_FUNCTION constexpr double sign(const double& v) { return v >= 0 ? 1.0 : -1.0; }

BlasKokkos::BlasHandle BlasKokkos::create() { return 0; }
void BlasKokkos::destroy(BlasKokkos::BlasHandle& /*handle*/){};

int BlasKokkos::gelsBatched(BlasKokkos::BlasHandle handle,
                            BatchedMatrix scratchMatrices,
                            BatchedPointers batchedMatrices,
                            BatchedVector scratchRHS,
                            BatchedPointers batchedRHS,
                            const int numModels,
                            const int numFeatures,
                            const int numSamples,
                            const int batchStartIndex,
                            DevInfo devInfo)
{
    if (numSamples > 4096) return EXIT_FAILURE;

    using POLICY_TYPE = Kokkos::TeamPolicy<>;
    auto policy = POLICY_TYPE(numModels, Kokkos::AUTO)
                      .set_scratch_size(0, Kokkos::PerTeam(sizeof(double) * numSamples));
    auto kernel = KOKKOS_LAMBDA(const typename POLICY_TYPE::member_type& team)
    {
        using namespace Kokkos;
        int ts = team.team_size();     // returns TEAM_SIZE
        int tid = team.team_rank();    // returns a number between 0 and TEAM_SIZE
        int ls = team.league_size();   // returns N
        int lid = team.league_rank();  // returns a number between 0 and N
        const int start = 0;

        const int stride = team.team_size();
        const int batchIdx = team.league_rank();

        auto policy = Kokkos::TeamThreadRange(team, 0, numSamples);

        double* const A = &scratchMatrices(0, 0, batchIdx);
        const int lda = scratchMatrices.stride_1();
        double* const b = &scratchRHS(0, batchIdx);
        auto v = Kokkos::View<double*, POLICY_TYPE::execution_space::scratch_memory_space>(
            team.team_scratch(0), numSamples);

        for (auto columnIdx = 0; columnIdx < numFeatures; ++columnIdx)
        {
            // copy column into v
            parallel_for(policy,
                         [&](const int& rowIdx)
                         { v[rowIdx] = rowIdx >= columnIdx ? A[rowIdx + columnIdx * lda] : 0.0; });

            // modify diagonal element
            double sumSquared = 0;
            parallel_reduce(
                policy,
                [&](const int& rowIdx, double& sum) { sum += v(rowIdx) * v(rowIdx); },
                Kokkos::Sum<double>(sumSquared));
            parallel_for(policy,
                         [&](const int& rowIdx)
                         {
                             if (rowIdx == columnIdx)  // only one element needs to be changed
                             {
                                 v[columnIdx] += sign(v[columnIdx]) * Kokkos::sqrt(sumSquared);
                             }
                         });

            // normalize
            sumSquared = 0;
            parallel_reduce(
                policy,
                [&](const int& rowIdx, double& sum) { sum += v(rowIdx) * v(rowIdx); },
                Kokkos::Sum<double>(sumSquared));
            auto invNorm = 1.0 / Kokkos::sqrt(sumSquared);
            parallel_for(policy, [&](const int& rowIdx) { v[rowIdx] *= invNorm; });

            // calc Q* b
            double dotB = 0;
            parallel_reduce(
                policy,
                [&](const int& rowIdx, double& sum) { sum += v(rowIdx) * b[rowIdx]; },
                Kokkos::Sum<double>(dotB));
            parallel_for(policy,
                         [&](const int& rowIdx)
                         {
                             if (rowIdx >= columnIdx)
                             {
                                 b[rowIdx] -= 2.0 * v[rowIdx] * dotB;
                             }
                         });

            for (auto innerColumnIdx = columnIdx; innerColumnIdx < numFeatures; ++innerColumnIdx)
            {
                // dot product to get length
                double v_dot_A = 0;
                parallel_reduce(
                    policy,
                    [&](const int& rowIdx, double& sum)
                    { sum += v(rowIdx) * A[rowIdx + innerColumnIdx * lda]; },
                    Kokkos::Sum<double>(v_dot_A));

                // subtract twice
                parallel_for(policy,
                             [&](const int& rowIdx)
                             { A[rowIdx + innerColumnIdx * lda] -= 2.0 * v[rowIdx] * v_dot_A; });
            }
        }

        // back substitution
        for (auto columnIdx = numFeatures - 1; columnIdx >= 0; --columnIdx)
        {
            parallel_for(policy,
                         [&](const int& rowIdx)
                         {
                             if (rowIdx == columnIdx)
                             {
                                 b[rowIdx] /= A[rowIdx + columnIdx * lda];
                             }
                         });
            team.team_barrier();
            parallel_for(policy,
                         [&](const int& rowIdx)
                         {
                             if (rowIdx < columnIdx)
                             {
                                 b[rowIdx] -= b[columnIdx] * A[rowIdx + columnIdx * lda];
                             }
                         });
        }
    };
    Kokkos::parallel_for("rank_models", policy, kernel);
    Kokkos::fence();

    return EXIT_SUCCESS;
}
