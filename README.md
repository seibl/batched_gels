# Build Instructions
## CPU
 * provide sequential and thread-safe BLAS/LAPACK libraries
 * MKL: `cmake -S . -B build -DBLA_VENDOR=Intel10_64lp_seq`
 * OpenBLAS: `OPENBLAS_NUM_THREADS=1` for sequential execution
 * AOCL: `BLIS_NUM_THREADS=1` for sequential execution

## Cuda
 * `cmake -S . -B build -DKokkos_ENABLE_CUDA=ON -DKokkos_ARCH_AMPERE80=ON`

## HIP
 * `cmake -S . -B build -DKokkos_ENABLE_HIP=ON -DKokkos_ENABLE_ROCM=ON`
