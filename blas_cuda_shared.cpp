#include <cooperative_groups.h>
#include <cooperative_groups/memcpy_async.h>
#include <cuda_runtime.h>
#include <stdio.h>

#include <cassert>
#include <stdexcept>

#include "blas_cuda_shared.hpp"

// https://developer.nvidia.com/blog/cooperative-groups/
namespace cg = cooperative_groups;

__device__ double dot(double const* const a, double const* const b, int const n)
{
    __shared__ double ret;
    double val = 0;

    cg::thread_block g = cg::this_thread_block();

    auto stride = blockDim.x;
    for (int idx = threadIdx.x; idx < n; idx += stride)
    {
        val += a[idx] * b[idx];
    }

    auto tile32 = cg::tiled_partition<32>(g);
    for (int i = tile32.size() / 2; i > 0; i /= 2)
    {
        val += tile32.shfl_down(val, i);
    }

    if (g.thread_rank() == 0)
    {
        ret = 0;
    }
    g.sync();
    if (tile32.thread_rank() == 0)
    {
        atomicAdd(&ret, val);
    }
    g.sync();
    return ret;
}

__device__ constexpr double sign(const double& v) { return v >= 0 ? 1.0 : -1.0; }

template <int MAX_M, int MAX_N>
__global__ void customDgelsBatched(const int M,
                                   const int N,
                                   double* const Aarray[], /*Device pointer*/
                                   const int lda,
                                   double* const Carray[], /*Device pointer*/
                                   const int batchSize)
{
    assert(lda >= M);
    assert(MAX_M >= M);
    assert(MAX_N >= N);

    // const int rowIdx = threadIdx.x;
    const int stride = blockDim.x;
    const int batchIdx = blockIdx.y * gridDim.x + blockIdx.x;

    __shared__ double A[MAX_M * MAX_N];
    __shared__ double v[MAX_M];
    __shared__ double b[MAX_M];

    cg::thread_block this_block = cg::this_thread_block();
    for (auto columnIdx = 0; columnIdx < N; ++columnIdx)
    {
        cg::memcpy_async(this_block,
                         &A[columnIdx * lda],
                         &Aarray[batchIdx][columnIdx * lda],
                         sizeof(double) * M);
    }
    cg::memcpy_async(this_block, b, Carray[batchIdx], sizeof(double) * M);
    cg::wait(this_block);  // Wait for all copies to complete

    for (auto columnIdx = 0; columnIdx < N; ++columnIdx)
    {
        // copy column into v
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            v[rowIdx] = rowIdx >= columnIdx ? A[rowIdx + columnIdx * lda] : 0.0;
        }

        // modify diagonal element
        double sumSquared = dot(v, v, M);
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            if (rowIdx == columnIdx)  // only one element needs to be changed
            {
                v[columnIdx] += sign(v[columnIdx]) * sqrt(sumSquared);
            }
        }

        // normalize
        sumSquared = dot(v, v, M);
        auto invNorm = rsqrt(sumSquared);
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            v[rowIdx] *= invNorm;
        }

        // calc Q* b
        double dotB = dot(v, b, M);
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            if (rowIdx >= columnIdx)
            {
                b[rowIdx] -= 2.0 * v[rowIdx] * dotB;
            }
        }

        for (auto innerColumnIdx = columnIdx; innerColumnIdx < N; ++innerColumnIdx)
        {
            // dot product to get length
            double v_dot_A = dot(v, &A[innerColumnIdx * lda], M);

            // subtract twice
            for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
            {
                A[rowIdx + innerColumnIdx * lda] -= 2.0 * v[rowIdx] * v_dot_A;
            }
        }
    }

    // back substitution
    for (auto columnIdx = N - 1; columnIdx >= 0; --columnIdx)
    {
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            if (rowIdx == columnIdx)
            {
                b[rowIdx] /= A[rowIdx + columnIdx * lda];
            }
        }
        this_block.sync();
        for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
        {
            if (rowIdx < columnIdx)
            {
                b[rowIdx] -= b[columnIdx] * A[rowIdx + columnIdx * lda];
            }
        }
    }

    for (int rowIdx = threadIdx.x; rowIdx < M; rowIdx += stride)
    {
        for (auto columnIdx = 0; columnIdx < N; ++columnIdx)
        {
            Aarray[batchIdx][rowIdx + columnIdx * lda] = A[rowIdx + columnIdx * lda];
        }
        Carray[batchIdx][rowIdx] = b[rowIdx];
    }
}

BlasCudaShared::BlasHandle BlasCudaShared::create() { return 0; }
void BlasCudaShared::destroy(BlasCudaShared::BlasHandle& /*handle*/) {}

int BlasCudaShared::gelsBatched(BlasHandle handle,
                                BatchedMatrix scratchMatrices,
                                BatchedPointers batchedMatrices,
                                BatchedVector scratchRHS,
                                BatchedPointers batchedRHS,
                                const int numModels,
                                const int numFeatures,
                                const int numSamples,
                                const int batchStartIndex,
                                DevInfo devInfo)
{
    if (numFeatures != 3) return EXIT_FAILURE;

    auto trans = 'N';
    auto m = numSamples;
    auto n = numFeatures;
    auto nrhs = 1;
    auto Aarray = reinterpret_cast<double**>(&batchedMatrices(batchStartIndex));
    int lda = scratchMatrices.stride_1();
    auto Carray = reinterpret_cast<double**>(&batchedRHS(batchStartIndex));
    int ldc = scratchRHS.stride_1();
    int info = 0;
    auto batchSize = numModels;

    if (numSamples <= 512)
    {
        auto threads = std::min(m, 256);
        customDgelsBatched<512, 3><<<batchSize, threads>>>(m, n, Aarray, lda, Carray, batchSize);
    }
    else if (numSamples <= 1024)
    {
        auto threads = std::min(numSamples, 512);
        customDgelsBatched<1024, 3><<<batchSize, threads>>>(m, n, Aarray, lda, Carray, batchSize);
    }
    else
    {
        return EXIT_FAILURE;
    }
    cudaDeviceSynchronize();
    return EXIT_SUCCESS;
}
