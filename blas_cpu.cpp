/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include "blas_cpu.hpp"

#include <vector>

extern "C"
{
    void dgels_(char* trans,
                int* m,
                int* n,
                int* nrhs,
                double* a,
                int* lda,
                double* b,
                int* ldb,
                double* work,
                int* lwork,
                int* info);

    void sgels_(char* trans,
                int* m,
                int* n,
                int* nrhs,
                float* a,
                int* lda,
                float* b,
                int* ldb,
                float* work,
                int* lwork,
                int* info);
}

BlasCPU::BlasHandle BlasCPU::create() { return 0; }

void BlasCPU::destroy(BlasCPU::BlasHandle& handle) {}

int BlasCPU::gelsBatched(BlasHandle handle,
                         BatchedMatrix scratchMatrices,
                         BatchedPointers batchedMatrices,
                         BatchedVector scratchRHS,
                         BatchedPointers batchedRHS,
                         const int numModels,
                         const int numFeatures,
                         const int numSamples,
                         const int batchStartIndex,
                         DevInfo devInfo)
{
    int lwork = 4096;
    std::vector<double> work(lwork, 0);

    auto trans = 'N';
    auto m = numSamples;
    auto n = numFeatures;
    auto nrhs = 1;
    auto Aarray = reinterpret_cast<double**>(&batchedMatrices(batchStartIndex));
    int lda = scratchMatrices.stride_1();
    auto Carray = reinterpret_cast<double**>(&batchedRHS(batchStartIndex));
    int ldc = scratchRHS.stride_1();
    int info = 0;
    auto batchSize = numModels;

#pragma omp parallel for firstprivate(trans,           \
                                      m,               \
                                      n,               \
                                      nrhs,            \
                                      Aarray,          \
                                      lda,             \
                                      Carray,          \
                                      ldc,             \
                                      info,            \
                                      batchSize,       \
                                      work,            \
                                      lwork,           \
                                      scratchMatrices, \
                                      scratchRHS) default(none)
    for (auto idx = 0; idx < batchSize; ++idx)
    {
        ::dgels_(&trans,
                 &m,
                 &n,
                 &nrhs,
                 Aarray[idx],
                 &lda,
                 Carray[idx],
                 &ldc,
                 work.data(),
                 &lwork,
                 &info);
    }
    return info;
}