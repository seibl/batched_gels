/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include "blas_rocsolver.hpp"

#define CHECK_HIP_ERROR(ans)                        \
    {                                               \
        check_hip_error((ans), __FILE__, __LINE__); \
    }
inline void check_hip_error(hipError_t code, const char* file, int line, bool abort = true)
{
    if (code != hipSuccess)
    {
        std::cerr << fmt::format("HIP ERROR: {} {} {}", hipGetErrorString(code), file, line)
                  << std::endl;
        if (abort) exit(code);
    }
}

BlasRocSolver::BlasHandle BlasRocSolver::create()
{
    rocblas_handle handle;
    rocblas_create_handle(&handle);
    return handle;
}

void BlasRocSolver::destroy(BlasRocSolver::BlasHandle& handle) { rocblas_destroy_handle(handle); }

int BlasRocSolver::gelsBatched(BlasRocSolver::BlasHandle handle,
                               BatchedMatrix scratchMatrices,
                               BatchedPointers batchedMatrices,
                               BatchedVector scratchRHS,
                               BatchedPointers batchedRHS,
                               const int numModels,
                               const int numFeatures,
                               const int numSamples,
                               const int batchStartIndex,
                               DevInfo devInfo)
{
    auto m = numSamples;
    auto n = numFeatures;
    auto nrhs = 1;
    auto Aarray = reinterpret_cast<double**>(&batchedMatrices(batchStartIndex));
    int lda = scratchMatrices.stride_1();
    auto Carray = reinterpret_cast<double**>(&batchedRHS(batchStartIndex));
    int ldc = scratchRHS.stride_1();
    int info = 0;
    auto batchSize = numModels;

    auto status = rocsolver_dgels_batched(
        handle, rocblas_operation_none, m, n, nrhs, Aarray, lda, Carray, ldc, devInfo.data(), batchSize);
    CHECK_HIP_ERROR(hipDeviceSynchronize());
    return status;
}
